'''
Created on May 20, 2015

@author: jonathancumpson
'''
import urlparse
import os.path

from twisted.web import http, proxy
from twisted.python import log
from twisted.internet import reactor
from twisted.application import internet
from twisted.internet.ssl import DefaultOpenSSLContextFactory
from twisted.internet.defer import logError
from twisted.internet.endpoints import TCP4ServerEndpoint, TCP4ClientEndpoint
from twisted.internet.protocol import ClientFactory, Protocol, connectionDone
from twisted.internet.error import ConnectionDone
from txsocksx.errors import ConnectionError

from pytunnel import locate
from txsocksx.client import SOCKS5ClientEndpoint


class HTTPSProxyTunnelClient(Protocol):
    connectedClient = None

    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        request = self.factory.proxyRequest
        request.channel.connectedRemote = self
        request.setResponseCode(200, 'CONNECT OK')
        request.setHeader('X-Connected-IP', self.transport.realAddress)
        request.setHeader('Content-Length', '0')
        host, _, _ = request.getHostPortProtocol()
        request.setHeader('Hostname', host)
        request.setHeader('Host', host)
        request.setHeader('X-Forwarded-Host', host)
        request.setHeader('X-Forwarded-Hostname', host)
        request.finish()

    def connectionLost(self, reason=connectionDone):
        if not reason.check(ConnectionDone):
            log.msg('client connection lost uncleanly: %s' % (reason,))
        if self.connectedClient is not None:
            self.connectedClient.transport.loseConnection()

    def dataReceived(self, data):
        if self.connectedClient is not None:
            return self.connectedClient.transport.write(data)
        log.err('Client received unexpected data: %r' % (data,))


class HTTPProxyClient(proxy.ProxyClient):
    connectedClient = None

    def __init__(self, factory):
        self._finished = False
        command = factory.method
        rest = factory.rest
        version = factory.clientproto
        headers = factory.headers
        data = factory.content
        father = factory.proxyRequest
        self.factory = factory

        proxy.ProxyClient.__init__(self, command, rest, version,
                                   headers, data, father)

        def handleResponseEnd(self):
            if not self._finished:
                self._finished = True
                try:
                    self.father.finish()
                except:
                    raise
            try:
                self.transport.loseConnection()
            except:
                pass


class RemoteClientFactory(ClientFactory):
    protocol = None

    def __init__(self, method, rest, clientproto, headers, content, request):
        self.method = method
        self.rest = rest
        self.clientproto = clientproto
        self.headers = headers
        self.content = content
        self.proxyRequest = request

    def clientConnectionFailed(self, connector, reason=ConnectionError):
        log.msg('client connection failed: %r, %s' % (connector, reason,))
        self.proxyRequest.fail('Gateway Error', '%s' % (reason,))

    def buildProtocol(self, _addr):
        return self.protocol(self)


PROTOCOLS = proxy.ProxyRequest.protocols.copy()
PROTOCOLS['https'] = RemoteClientFactory
PROTOCOLS['http'] = RemoteClientFactory
PORTS = proxy.ProxyRequest.ports.copy()
PORTS['https'] = 443


class ProxyRequest(http.Request):
    socksEndpoint = None
    protocols = PROTOCOLS
    ports = PORTS

    def getHostPortProtocol(self):
        parsed = urlparse.urlparse(self.uri)
        protocol = parsed[0]
        host = parsed[1]
        port = self.ports[protocol]
        if ':' in host:
            host, port = host.split(':')
            port = int(port)

        return host, port, protocol

    def getContent(self):
        self.content.seek(0, 0)
        o = self.content.read()
        self.content.seek(0, 0)
        return o

    def getRestfulUrl(self):
        parsed = urlparse.urlparse(self.uri)
        rest = urlparse.urlunparse(('', '') + parsed[2:])
        if not rest:
            rest = rest + '/'

        return rest

    def requestReceived(self, command, path, version):
        log.msg('Request received: %s, %s, %s' % (command, path, version))

        # if the protocol is not prepended (chrome does this) then try to
        # figure out if it is supposed to he an HTTPS connection or not
        if not path.startswith('http'):
            # if port 443, then ssl
            if '443' in path:
                path = 'https://%s' % (path,)
            else:
                path = 'http://%s' % (path,)
        log.msg('requestReceived [%s]' % (path,))

        return http.Request.requestReceived(self, command, path, version)

    def fail(self, message, body):
        log.msg('request failed: %s, %s, %s' % (self, message, body))
        self.setResponseCode(501, message)
        self.responseHeaders.addRawHeader('Content-Type', 'text/html')
        self.write(body)
        try:
            self.finish()
        except:
            log.err()

    def process(self):
        host, port, protocol = self.getHostPortProtocol()
        rest = self.getRestfulUrl()

        if not host or not port or not protocol:
            return self.fail('Bad Request', 'Unable to parse URI: %r' \
                             % (self.uri,))

        socks_endpoint = getattr(self.channel.factory, 'socksEndpoint', None)
        log.msg('socks_endpoint [%s]' % (socks_endpoint,))
        log.msg('host, port, protocol (%s, %s, %s)' % (host, port, protocol,))

        if int(port) == 443:
            endpoint = SOCKS5ClientEndpoint(host, port, socks_endpoint)
            protocol = HTTPSProxyTunnelClient
        else:
            endpoint = TCP4ClientEndpoint(reactor, host, port)
            protocol = HTTPProxyClient

        headers = self.getAllHeaders().copy()
        content = self.getContent()

        if 'host' not in headers:
            headers['host'] = host

        clientFactory = RemoteClientFactory(self.method,
                                            rest,
                                            self.clientproto,
                                            headers,
                                            content,
                                            self
                                            )
        clientFactory.protocol = protocol
        self.connected = endpoint.connect(clientFactory)
        self.connected.addErrback(logError)


class ProxyProtocol(proxy.Proxy):
    connectedRemote = None
    requestFactory = ProxyRequest
    keypath = None
    crtpath = None

    def __init__(self, *args, **kwargs):
        proxy.Proxy.__init__(self, *args, **kwargs)

        if self.keypath is None:
            dirpath = os.path.abspath(locate.path())
            keypath = os.path.join(dirpath, 'ssl.key')
            crtpath = os.path.join(dirpath, 'ssl.crt')

            if not os.path.exists(keypath):
                raise Exception('Invalid ssl-key path, does not exist: %s'
                                % (keypath,))

            if not os.path.exists(crtpath):
                raise Exception('Invalid ssl-certificate path, '
                                'does not exist: %s' % (crtpath,))
            self.keypath = keypath
            self.crtpath = crtpath

        self._contextFactory = DefaultOpenSSLContextFactory(self.keypath,
                                                            self.crtpath)

    def requestDone(self, request):
        if request.method == 'CONNECT' and self.connectedRemote is not None:
            self.connectedRemote.connectedClient = self
        else:
            proxy.Proxy.requestDone(self, request)

    def connectionLost(self, reason):
        if self.connectedRemote is not None:
            self.connectedRemote.transport.loseConnection()
        return proxy.Proxy.connectionLost(self, reason)

    def connectionMade(self):
        log.msg('connection made: %s' % (self,))
        proxy.Proxy.connectionMade(self)

    def dataReceived(self, data):
        if self.connectedRemote is not None:
            return self.connectedRemote.transport.write(data)
        return proxy.Proxy.dataReceived(self, data)


def makeService(socks_endpoint,
                port=14670,
                protocolClass=ProxyProtocol,
                factoryClass=http.HTTPFactory):
    factory = factoryClass()
    factory.socksEndpoint = socks_endpoint
    factory.protocol = protocolClass

    endpoint = TCP4ServerEndpoint(reactor, port)
    return internet.StreamServerEndpointService(endpoint, factory)
