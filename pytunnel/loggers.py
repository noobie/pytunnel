'''
Created on Apr 28, 2015

@author: jonathancumpson
'''
import os.path
from twisted.python.logfile import DailyLogFile

from pytunnel import locate
from twisted.python.log import FileLogObserver


path = os.path.abspath(os.path.join(locate.path()))


def dailylogger():
    return FileLogObserver(DailyLogFile('pytunnel.log', path)).emit
