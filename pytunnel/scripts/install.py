'''
Created on Apr 29, 2015

@author: jonathancumpson
'''
import os.path
from pytunnel import locate
import subprocess


def _run_cmd(cmd):
    proc = subprocess.Popen(cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,)
    stdout, stderr = proc.communicate()
    if stdout:
        print stdout
    if stderr:
        print 'ERROR: %s' % (stderr,)
    return proc


def install():
    path = locate.path()
    service_exe = os.path.join(path, 'service.exe')
    cmd = '%s --startup delayed install' % (service_exe,)
    _run_cmd(cmd)
    cmd = '%s start' % (service_exe,)
    _run_cmd(cmd)


if __name__ == "__main__":
    install()
