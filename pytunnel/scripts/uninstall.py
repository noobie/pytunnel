'''
Created on Apr 29, 2015

@author: jonathancumpson
'''
import os.path
from pytunnel import locate
from pytunnel.scripts.install import _run_cmd


def uninstall():
    path = locate.path()
    service_exe = os.path.join(path, 'service.exe')
    cmd = '%s stop' % (service_exe,)
    _run_cmd(cmd)
    cmd = '%s remove' % (service_exe,)
    _run_cmd(cmd)


if __name__ == "__main__":
    uninstall()
