'''
Created on Apr 29, 2015

@author: jonathancumpson
'''
from twisted.python.runtime import platformType


if platformType == 'win32':
    from twisted.scripts._twistw import ServerOptions
    from twisted.scripts._twistw import WindowsApplicationRunner as \
                                            _SomeApplicationRunner
else:
    from twisted.scripts._twistd_unix import ServerOptions
    from twisted.scripts._twistd_unix import UnixApplicationRunner as \
                                                _SomeApplicationRunner


class ApplicationRunner(_SomeApplicationRunner):
    def createOrGetApplication(self):
        from twisted.application import service
        from twisted.python.log import ILogObserver

        from pytunnel.tap import PytunnelOptions, serviceMaker
        from pytunnel.loggers import dailylogger

        opts = PytunnelOptions()
        opts.parseOptions()

        s = serviceMaker.makeService(opts)
        application = service.Application('pytunnel-tac')
        application.setComponent(ILogObserver, dailylogger())
        s.setServiceParent(application)
        return application


def run():
    config = ServerOptions()
    ApplicationRunner(config).run()


if __name__ == '__main__':
    run()
