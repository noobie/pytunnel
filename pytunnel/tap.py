'''
Created on Apr 28, 2015

@author: jonathancumpson
'''
import os
import sys
from ConfigParser import SafeConfigParser, NoSectionError, NoOptionError

from zope.interface import implements
from twisted.plugin import IPlugin
from twisted.python.usage import Options
from twisted.python import log
from twisted.runner.procmon import LoggingProtocol
from twisted.internet.threads import deferToThread
from twisted.application.service import IServiceMaker, MultiService

import pytunnel
from pytunnel import locate
from pytunnel.proxy import protocol
from twisted.internet.endpoints import TCP4ClientEndpoint


class PytunnelOptions(Options):
    optFlags = [
        ['log-stdout', 'L', 'Log to stdout instead of logfile'],
        ['interactive', 'I', 'Set the service to interactive mode. Allows '
         'users to type in the terminal'],
        ['debugging', None, 'Allow debugging logs'],
        ]

    optParameters = [
     ['log-file', 'l', 'pytunnel.log', 'File to log dataz to'],
     ['session', 's', None, 'Saved PuTTY session to use - see '
      'plink/putty usage'],
     ['private-key', 'k', None,
      'The private key to login with to the server'],
     ['config', 'c', None, 'A config file to load for infoz'],
     ['executable', 'x', None, 'The putty.exe or plink.exe executable to use'],
     ['executable-args', 'X', None, 'Arguments to pass to the .exe'],
     ['executable-custom', 'T', None, 'Completely override the .exe with '
      'this argument string'],
     ['proxy-host', 'H', None, 'Proxy host to connect to, ie. your remote '
      'proxy server'],
     ['proxy-port', 'P', None, 'Proxy host listening port,ie. the remote '
      'proxies SSH port', int],
     ['interface', 'i', None, 'Listening interface (default=0.0.0.0)'],
     ['port', 'p', None, 'Listening port, this is what your applications can '
      'connect to locally (default=14669)', int],
     ['password', 'W', None, 'Password instead of private-key '
      'NOT RECOMMENDED!'],
     ['https-port', None, None, 'Listening port for http/https connetions, '
      'none by default', int],
     ['https-ssl-key', None, None, 'The key for your https serving'],
     ['https-ssl-cert', None, None, 'The certificate for https serving']
     ]


class ServiceMaker(object):

    implements(IServiceMaker, IPlugin)

    tapname = 'pytunnel'
    description = 'A python-powered tunneling service'
    version = pytunnel.__version__
    options = PytunnelOptions

    def _startTunnel(self, options):
        from twisted.internet import reactor
        log.msg('locat path: %s' % (locate.path(),))

        config = SafeConfigParser()
        dirpath = os.path.abspath(locate.path())
        binpath = os.path.join(dirpath, 'bin')

        #--- load the default config provided with the service
        config_path = options.get('config')
        if config_path is None:
            config_path = os.path.join(dirpath, 'configure.conf')
        config.read([config_path, ])

        #--- read the alternate config if it was provided in the original options
        try:
            alternate = config.get('alternate', 'path')
            log.msg('Using alternate config: %s' % (alternate,))
            alternate = os.path.abspath(alternate)
            if not os.path.exists(alternate):
                log.msg('Aborting, alternate config path does not exist: %s' \
                        % (alternate,))
            config = SafeConfigParser()
            config.read([alternate, ])
        except (NoSectionError, NoOptionError):
            pass
        except:
            # if there is a different error, log it and exit something bad
            # happened
            log.err()
            reactor.stop()

        #--- locate the executable to use with the service
        executable = options.get('executable')
        if executable is None:
            try:
                executable = config.get('executable', 'path')
            except (NoSectionError, NoOptionError):
                executable = os.path.join(binpath, 'plink.exe')
            except:
                log.err()
                reactor.stop()

        #--- locate the private-key or use interactive mode for the service
        privatekey = options.get('private-key')
        if privatekey is None:
            try:
                privatekey = config.get('proxy', 'private_key')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        #--- validate the private key path
        if privatekey is not None:
            privatekey = os.path.abspath(privatekey)
            if not os.path.exists(privatekey):
                privatekey = None

        password = options.get('password')
        if password is None:
            try:
                password = config.get('proxy', 'password')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        # if no private key is provided and interactive mode isn't enabled
        # then there is no way for the proxy to authenticate connections, so
        # log error and exit
        proxyhost = options.get('proxy-host')
        if proxyhost is None:
            try:
                proxyhost = config.get('proxy', 'host')
            except (NoSectionError, NoOptionError):
                pass
                log.err()
                reactor.stop()

        proxyport = options.get('proxy-port')
        if proxyport is None:
            try:
                proxyport = config.get('proxy', 'port')
            except (NoSectionError, NoOptionError):
                proxyport = 22
            except:
                log.err()
                reactor.stop()

        interface = options.get('interface')
        if interface is None:
            try:
                interface = config.get('proxy', 'interface')
            except (NoSectionError, NoOptionError):
                interface = '0.0.0.0'
            except:
                log.err()
                reactor.stop()

        port = options.get('port')
        if port is None:
            try:
                port = config.get('local', 'port')
            except (NoSectionError, NoOptionError):
                port = 14669
            except:
                log.err()
                reactor.stop()

        https_port = options.get('https-port')
        if https_port is None:
            try:
                https_port = int(config.get('https', 'port'))
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        https_key = options.get('https-ssl-key')
        if https_key is None:
            try:
                https_key = config.get('https', 'key')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        https_cert = options.get('https-ssl-cert')
        if https_cert is None:
            try:
                https_cert = config.get('https', 'cert')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        execargs = options.get('executable-args')
        if execargs is None:
            try:
                config.get('executable', 'args')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        customargs = options.get('executable-custom')
        if customargs is None:
            try:
                config.get('executable', 'custom')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        session = options.get('session')
        if session is None:
            try:
                session = config.get('exectuable', 'session')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        interactive = options.get('interactive')
        if interactive is None:
            try:
                interactive = config.get('proxy', 'interactive')
            except (NoSectionError, NoOptionError):
                pass
            except:
                log.err()
                reactor.stop()

        if not executable:
            log.msg('Cannot create tunnel service, no executable provided')
            reactor.stop()
        if not proxyhost:
            log.msg('Cannot create tunnel service, no proxy host provided')
            reactor.stop()
        if not proxyport:
            log.msg('Cannot create tunnel service, no proxy port provided')
            reactor.stop()
        if not port:
            log.msg('Cannot create tunnel service, no local port provided')
            reactor.stop()

        # using the procmon package to actually run the plink/putty executable
        # instead of trying to manage that
        from twisted.runner import procmontap
        args = [executable, '-agent', ]

        if customargs:
            log.msg('Option \'customargs\' used, ignoring other parameters')
            args.extend(customargs)
        else:
            if session:
                log.msg('Option \'session\' used, ignoring other parameters')
                args.extend(['-load', session])

            else:
                if proxyhost:
                    args.extend([proxyhost])

                if password:
                    args.extend(['-pw', password])
                elif privatekey:
                    args.extend(['-i', privatekey])

                if proxyport:
                    args.extend(['-P', str(proxyport)])

#                 if not interactive:
#                     args.extend(['-N'])

                args.extend(['-D', str(port)])

            if execargs:
                args.extend(execargs)

        if options['debugging']:
            log.msg('Starting %s with args: %s' % (executable, args,))

        procopts = procmontap.Options()
        procopts.parseOptions(args)
        s = procmontap.makeService(procopts)

        # poopy monkey patching
        LoggingProtocol._data = ''
        LoggingProtocol._fingerprintdone = False
#         outReceived = LoggingProtocol.outReceived

        def overrideFingerprintError(self, data):
            if not interactive and 'password' in data:
                log.msg('Cannot enter password, not interactive mode. '
                        'Exiting.')
                reactor.stop()

            elif 'password' in data:
                # XXX: currently twisted has a bug with windows that does
                # not allow stdin to be read from the reactor :(
                log.msg('Interactive mode is not supported due to bug! :(')
                reactor.stop()
                return

                def getPassword():
                    log.msg('Enter password:')
                    return raw_input()

                d = deferToThread(getPassword)
                d.addCallback(lambda pw: self.transport.write(pw + '\r\n'))

            elif 'Last login' in data:
                self.fingerprintdone = True
                log.msg('Connected to proxy server [%s:%s]' % \
                        (proxyhost, proxyport,))

            if self._fingerprintdone:
                return

            self._data += data
            if 'fingerprint' in self._data:
                self._fingerprintdone = True
                self._data = ''
                self.transport.write('y\r\n')

        LoggingProtocol.outReceived = lambda self, data: \
            overrideFingerprintError(self, data)
        LoggingProtocol.errReceived = LoggingProtocol.outReceived

        s.setName('proxy-executable')
        if options['debugging']:
            pass

        try:
            s.setServiceParent(self.service)
        except:
            log.err()
            reactor.stop()

        if https_port:
            endpoint = TCP4ClientEndpoint(reactor, 'localhost', port)
            protocolClass = protocol.ProxyProtocol

            if https_key:
                protocolClass.keypath = https_key

            if https_cert:
                protocolClass.certpath = https_cert

            if https_cert or https_key:
                s = protocol.makeService(endpoint, https_port,
                                         protocolClass=protocolClass)
            else:
                s = protocol.makeService(endpoint, https_port)
            s.setName('http/https proxy')
            s.setServiceParent(self.service)

    def _makeService(self, options, service=None,):
        if service is None:
            service = MultiService()
            service.setName(self.tapname)

        self.service = service
        service.maker = self
        self._startTunnel(options)

    def makeService(self, options):
        from twisted.internet import reactor
        s = MultiService()
        s.setName(self.tapname)
        reactor.callWhenRunning(self._makeService, options=options, service=s)
        return s


serviceMaker = ServiceMaker()
makeService = serviceMaker.makeService
