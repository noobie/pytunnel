import signal
import os.path
import win32event
import subprocess
import win32service
import win32serviceutil
from time import sleep

from pytunnel import tap


class PySvc(win32serviceutil.ServiceFramework):
    # you can NET START/STOP the service by the following name
    _svc_name_ = tap.ServiceMaker.tapname
    # this text shows up as the service name in the Service
    # Control Manager (SCM)
    _svc_display_name_ = tap.ServiceMaker.tapname
    # this text shows up as the description in the SCM
    _svc_description_ = tap.ServiceMaker.description

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        # create an event to listen for stop requests on
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

    # core logic of the service
    def SvcDoRun(self):
        from pytunnel.scripts import app
        app.run()

    # called when being shut down
    def SvcStop(self):
        # tell the SCM that service is shutting down
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # fire the stop event
        win32event.SetEvent(self.hWaitStop)
        # stop the twisted reactor and app
        from twisted.internet import reactor
        reactor.stop()


if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(PySvc)
