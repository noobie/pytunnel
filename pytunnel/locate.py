'''
Created on Sep 3, 2013

@author: noobie
'''
import os
import sys


def path():
    encoding = sys.getfilesystemencoding()
    if hasattr(sys, 'frozen'):
        return os.path.dirname(unicode(sys.executable, encoding))
    return os.path.dirname(os.path.dirname(unicode(__file__, encoding)))
