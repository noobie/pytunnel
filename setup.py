import os

from setuptools import setup, find_packages
try:
    import pip
except ImportError:
    print 'ERROR: exiting setup. Pip is required to find dependencies '\
            'and was not able to be imported. If you have easy_install ' \
            'type \'easy_install pip\''
    exit()

major, minor, micro = map(int, pip.__version__.split('.'))
if major < 6:
    raise Exception('Requires pip version 6.0.0 or later please update pip')


from pytunnel import __version__


requirements_path = os.path.join(os.path.dirname(__file__), 'pip_requires.txt')

if not os.path.exists(requirements_path):
    print 'ERROR: exiting setup. Pip requires a requirements file to check '\
            'dependencies. The expected path \'%s\' does not exist.' \
            % (os.path.abspath(requirements_path),)
    exit()

pip_install_requires = pip.req.parse_requirements(requirements_path,
                                                  session=pip.download.PipSession())


def get_requirement(req):
    if req.req is not None:
        return "%s" % (req.req,)
    elif req.url is not None:
        return '=='.join(req.url.rsplit('/', 1)[1].rsplit('-', 1))
    else:
        print 'ERROR: cannot get requirement for %r' % (req,)


requirements = filter(None, map(get_requirement, pip_install_requires))

setup(version=__version__,
      name='pytunnel',
      author='JD Cumpson',
      author_email='cumpsonjd@gmail.com',
      zip_safe=False,
      install_requires=requirements,
      packages=find_packages(exclude=[]),
      include_package_data=True,
      )
