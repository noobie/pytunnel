'''
Created on Aug 30, 2013

@author: noobie
'''
import os
import sys
from distutils.core import setup
from setuptools import find_packages
from glob import glob

import py2exe
import pytunnel


class Target(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.version = pytunnel.__version__

        self.company_name = "JD Cumpson"
        self.copyright = "MIT License"
        self.name = "pytunnel"

from pytunnel.service import PySvc
pts = Target(description=PySvc._svc_description_,
             modules='pytunnel.service',
             cmdline_style='pywin32'
             )

if sys.platform != 'win32':
    raise Exception('You can\'t make an exe without windows!')

winversion = '32'
if 'AMD64' in sys.version:
    winversion = '-amd64'

pkg = os.path.abspath('pytunnel')
app = os.path.join(pkg, 'scripts', 'app.py')
install = os.path.join(pkg, 'scripts', 'install.py')
uninstall = os.path.join(pkg, 'scripts', 'uninstall.py')
packages = find_packages()
includes = ['pkg_resources', 'setuptools', 'twisted', 'urllib', 'Crypto']

setup(console=[app, install, uninstall, ],
      service=[pts],
      version=pytunnel.__version__,
      name='pytunnel',
      author='JD Cumpson',
      author_email='cumpsonjd@gmail.com',
      zip_safe=False,
      packages=packages,
      data_files=['configure.conf', ('bin', glob('bin\*')), 'ssl.key',
                  'ssl.crt'],
      zipfile=None,
      options={'py2exe':{'dist_dir':'pytunnel-%s-win%s' % \
                         (pytunnel.__version__, winversion),
                         }
               },
      )
