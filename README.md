pytunnel
=========

**COMING SOON**: http/https -> SOCKS5 proxy plugin for non-socks5 capable devices.


Use this application to setup a service in windows that runs a plink.exe or 
putty.exe application to establish an SSH SOCKS5 tunnel.
Check the downloads section of the project, if your operating system is 
available please download the zip file that corresponds to your system.


## Installation:
---
1. Go into the pytunnel directory, pytunnel-<version>-<os-type>
2. Edit the configure.conf to setup your proxy information, see Configuration
3. To install run, `install.exe`
4. To remove run, `uninstall.exe`

## Configuration
---
The configuration file is provided as configure.conf. All the default options
are available (commented out).

Sections for the configure.conf file are:

- **alternate**
- **proxy**
- **executable**
- **local**

## Sections
---
##### alternate
- **path** = an alternate configuaration file to use, this one will be ignored

##### proxy
- **private_key** = the path to your SSH private key (.ppk)
- **port** = the remote proxy connection port
- **host** = the remote proxy connection hostname
- **password** = a plain-text password to pass along instead of a private key, USE AT YOUR OWN RISK
- **interactive** = (NOT AVAILABLE) allow the executable to be run in
                    interactive mode
##### executable
- **path** = the path to the plink.exe exectuble to be used as the transport
- **session** = the named putty session to use to connect with, other config options will be ignored if this parameter is used
- **args** = additional plink.exe arguments to pass along manually, does NOT override other commands
- **custom** = a completely custom argument set to pass to plink.exe, other arguments will be ignored if this parameter is passed

##### local
- **port** = the local port you wish to listen on with plink (default=14669)
- - -


Also you may run the `app.exe` by hand (not as a service). If you want to do this
and use your own batch scripts you are welcome. You may use the `app.exe --help` 
command to find out configurable options. 

**NOTE**: Arguments passed to the executable take precedence over config options
in a configuration file.



## Build From Source:
---

1. Checkout the git project
```bash
$git clone git@bitbucket.org:noobie/pytunnel.git
```

2. Make sure you have Python2.6+ installed, see [Python2.7](https://www.python.org/downloads/release/python-279/)
3. Make sure you have pip installed, download and run [get-pip.py](https://bootstrap.pypa.io/get-pip.py)


4. Make sure your python instance has the appropriate packages
```bash
$pip install -r pip_requires.txt
```
5. Build the py2exe executable
```bash
$python py2exe.setup.py py2exe
```